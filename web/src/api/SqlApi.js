import {Axios} from '../utils/AxiosPlugin'

const runSql = async function (configName, sql) {
  return await Axios.post('/sql/runSql', {configName: configName, sql: sql});
};

export default {
  runSql
}
