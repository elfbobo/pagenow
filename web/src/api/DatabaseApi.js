import {Axios} from '../utils/AxiosPlugin'

const getAllDatabase = async function () {
  return await Axios.get('/database/getAllDatabase');
};

export default {
  getAllDatabase
}
