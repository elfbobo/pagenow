
const AbsoluteLayoutGlobalMixin = {
  data() {
    return {

    }
  },
  created () {

  },
  methods: {
    buildEnterActiveClass (layoutItem) {
      if(layoutItem.layoutItemConfigData.animationVisible) {
        return 'animated ' + layoutItem.layoutItemConfigData.inAnimation + ' ' + layoutItem.layoutItemConfigData.animationDelay;
      }else {
        return ''
      }
    },
    buildLeaveActiveClass (layoutItem) {
      if(layoutItem.layoutItemConfigData.animationVisible) {
        return 'animated ' + layoutItem.layoutItemConfigData.outAnimation + ' ' + layoutItem.layoutItemConfigData.animationDelay;
      }else {
        return ''
      }
    },

    buildLayoutItemTransformRotate (layoutItem) {
      let flipStyle = '';
      let flipHorizontalStyle = '';
      let flipVerticalStyle = '';
      if (layoutItem.layoutItemConfigData.flipHorizontal) {
        flipHorizontalStyle  = 'rotateY(180deg)'
      }else {
        flipHorizontalStyle = ''
      }
      if (layoutItem.layoutItemConfigData.flipVertical) {
        flipVerticalStyle = 'rotateX(180deg)'
      }else {
        flipVerticalStyle = ''
      }

      if(flipHorizontalStyle && flipVerticalStyle) {
        flipStyle = flipHorizontalStyle + ' ' + flipVerticalStyle + ' ' + 'rotate(' + layoutItem.layoutItemConfigData.rotationAngle + 'deg)';
      }else if(flipHorizontalStyle && !flipVerticalStyle) {
        flipStyle = flipHorizontalStyle + ' ' + 'rotate(' + layoutItem.layoutItemConfigData.rotationAngle + 'deg)';
      }else if(!flipHorizontalStyle && flipVerticalStyle) {
        flipStyle = flipVerticalStyle + ' ' + 'rotate(' + layoutItem.layoutItemConfigData.rotationAngle + 'deg)';
      }else {
        flipStyle = 'rotate(' + layoutItem.layoutItemConfigData.rotationAngle + 'deg)';
      }

      return flipStyle
    }
  },
  computed: {
    canvasBackgroundImageSrc () {
      return process.env.VUE_APP_BASEPATH + '/' + this.layout.layoutConfigData.imageRelativePath
    }
  }
};

export default AbsoluteLayoutGlobalMixin
