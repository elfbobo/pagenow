
const LayoutItemFormMixin = {
  data() {
    return {

    }
  },
  created () {

  },
  computed: {
    buildBaseLayoutItemFormName () {
      let pageMetadata = this.$store.state.designer.pageMetadata;
      if (pageMetadata.developCanvas == 'AbsoluteLayoutCanvas') {
        return 'AbsoluteLayoutItemForm'
      }else if (pageMetadata.developCanvas == 'ReactiveLayoutCanvas') {
        return 'ReactiveLayoutItemForm'
      }
      return ''
    },
  }
};

export default LayoutItemFormMixin
