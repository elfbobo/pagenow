// 功能组件混入对象

const FuncCompMixin = {
  props: {
    location: {
      type: String,
      default: ''
    }
  },
  created () {},
  destroyed () {

    // 清除定时器
    if(this.timer) {
      clearInterval(this.timer);
    }

  },
  data () {
    return {
      timer: null
    }
  },
  methods: {

    /**
     * 设置定时器
     * @param callback
     */
    setTimer (callback) {
      if(this.$store.state.release.pageMetadata) {
        if (this.component.compConfigData.ds_autoRefresh) {
          this.timer = setInterval(function () {
            if (callback && typeof callback === 'function') {
              callback()
            }
          }, this.component.compConfigData.ds_autoRefreshDuration * 1000)
        }
      }
    }

  },
  computed: {
    projectInfo () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getProjectInfo']
      }else {
        return this.$store.getters['designer/getProjectInfo']
      }
    },
    pageMetadata () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getPageMetadata']
      }else {
        return this.$store.getters['designer/getPageMetadata']
      }
    },
    component: function () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getLayoutItemById'](this.location).component
      }else {
        return this.$store.getters['designer/getLayoutItemById'](this.location).component
      }
    },

    /**
     * 是否显示布局块遮罩层
     * @returns {boolean}
     */
    shade () {
      if(this.$store.state.release.pageMetadata) {
        return false
      }else {
        return true;
      }
    },

    defaultImg () {
      return 'this.src="'+require('@/assets/image/default-img.png')+'";this.onerror=null'
    }
  }
};

export default FuncCompMixin
