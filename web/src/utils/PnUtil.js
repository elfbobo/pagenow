/* eslint-disable no-useless-escape */
import router from '../router'

/**
 * 生成UUID
 * @returns {string}
 */
const uuid = function() {
  function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }
  return (S4() + S4() + "" + S4() + "" + S4() + "" + S4() + "" + S4() + S4() + S4());
};

/**
 * 获取当前时间戳
 * @returns {number}
 */
const getTimestamp = function () {
  return new Date().getTime();
};

/**
 * 打开一个新的页面
 * @param path
 * @param query
 */
const openPageToBlank = function (path, query) {
  let routeUrl = router.resolve({
    path: path,
    query: query
  });
  window.open(routeUrl.href, '_blank');
};

/**
 * 获取反色差值
 * @param hexcolor (示例：FFFFFF)
 * @returns {string}
 */
const getContrastYIQ = function (hexcolor) {
  let r = parseInt(hexcolor.substr(0,2),16);
  let g = parseInt(hexcolor.substr(2,2),16);
  let b = parseInt(hexcolor.substr(4,2),16);
  let yiq = ((r*299)+(g*587)+(b*114))/1000;
  return (yiq >= 128) ? 'black' : 'white';
};

/**
 * 判断是否为Mac系统
 * @returns {*|boolean}
 */
const isMac = function () {
  return /macintosh|mac os x/i.test(navigator.userAgent);
};

/**
 * 判断是否为Windows系统
 * @returns {*|boolean}
 */
const isWindows = function () {
  return /windows|win32/i.test(navigator.userAgent);
};

/**
 * 删除iView的Table中行数据的一些前端无用字段，提供给后端JFinal更新使用
 * @param obj
 * @returns {*}
 */
const deleteTableRowUselessField = function (obj) {
  if(obj) {
    let newObj = Object.assign({}, obj);
    delete newObj._index;
    delete newObj._rowKey;
    return newObj
  }
  return null
};

/**
 * css样式文本转Vue专用的Style对象
 * @param cssStr
 */
const cssToVueStyleObj = function (cssStr) {
  let styleObj = {};

  if (cssStr) {
    // 去除空格和换行
    cssStr = cssStr.replace(/\ +/g, '').replace(/[\r\n]/g, '');
    // 去除 '{' 和 '}'
    cssStr = cssStr.replace('{','').replace('}','');

    // 拆分样式项并组成样式项数组
    let cssItems = cssStr.split(';');

    cssItems.forEach(cssItem => {
      if(cssItem) {
        let attr = cssItem.split(':')[0]; // 样式项键
        let val = cssItem.split(':')[1];  // 样式项值

        // 定义一个数组存储属性键中'-'出现的位置
        let positions = [];
        let pos = attr.indexOf('-');
        while (pos > -1) {
          positions.push(pos);
          pos = attr.indexOf('-', pos + 1);
        }

        if(positions.length > 0) {
          // 定义一个数组存储每一次出现'-'符号时要处理的替换数据
          let replaceArr = [];
          for (let i=0; i<positions.length; i++) {
            // 待替换字符
            let awaitReplaceStr = attr.substring(positions[i], positions[i]+2);
            // 替换的字符
            let newStr = attr.substring(positions[i]+1, positions[i]+2);

            replaceArr.push({
              awaitReplaceStr: awaitReplaceStr,
              newStr: newStr.toLocaleUpperCase() //替换的字符要转成大写
            });
          }

          replaceArr.forEach(item=>{
            attr = attr.replace(item.awaitReplaceStr, item.newStr)
          })
        }

        styleObj[attr] = val

      }
    });
  }

  return styleObj;
};

/**
 * 深拷贝对象
 * @param obj
 * @returns {any}
 */
const deepClone = function (obj) {
  let objStr = JSON.stringify(obj);
  let cloneObj = JSON.parse(objStr);
  return cloneObj
};

/**
 * 深拷贝数组
 * @param arr
 * @returns {*}
 */
const recursiveClone = function (arr) {
  return Array.isArray(arr) ? Array.form(arr, recursiveClone) : arr;
};

/**
 * 动态添加URL参数
 * @param key
 * @param value
 */
const addUrlParams = function (key, value) {
  let uri = window.location.href;

  let re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  let separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    uri = uri.replace(re, '$1' + key + "=" + value + '$2');
  }else {
    uri = uri + separator + key + "=" + value;
  }
  window.history.replaceState({
    path: uri
  }, '', uri);
};

/**
 * 构建接口地址
 * @param apiPath 接口地址
 * @param linkageUrlParams 联动参数集合
 * @returns {*}
 */
const buildApiPath = function (apiPath, linkageUrlParams) {
  let params = '';
  for(let q in router.history.current.query) {
    linkageUrlParams.forEach(item=>{
      if (item.field == q) {
        params += '&' + q + '=' + router.history.current.query[q] + ''
      }
    });
  }
  params = params.substr(0, params.length-1);

  if (linkageUrlParams.length == 0) {
    return apiPath
  }else {
    if(apiPath.indexOf('?') > 0) {
      apiPath += params;
    }else {
      apiPath += '?' + params;
    }
    return apiPath;
  }
};

/**
 * 获取组件的compConfigData默认配置数据
 * @param compName
 * @returns {*}
 */
const getCompConfigData = function (compName) {
  let compConfigData = null;
  const componentsContext = require.context('../components/', true, /\.vue$/);

  componentsContext.keys().forEach(fileName => {
    const componentConfig = componentsContext(fileName);

    if(componentConfig.default.name == compName) {
      const compInst = require('../components/'+fileName.slice(2, fileName.length));
      compConfigData = Object.assign({}, compInst.default.attr.configDataTemp)
    }
  });

  return compConfigData
};

/**
 * 获取FunctionalComponents文件夹下所有功能组件的文件名（已排除掉了配置表单文件名）
 * @returns {Array}
 */
const getAllFuncCompName = function () {
  let arr = [];
  const componentsContext = require.context('../components/FunctionalComponents/', true, /\.vue$/);

  componentsContext.keys().forEach(fileName => {
    const componentConfig = componentsContext(fileName);
    if (componentConfig.default.name.indexOf('Form') < 0) {
      arr.push(componentConfig.default.name)
    }
  });
  return arr;
};

export default {
  uuid,
  getTimestamp,
  openPageToBlank,
  getContrastYIQ,
  isMac,
  isWindows,
  deleteTableRowUselessField,
  cssToVueStyleObj,
  deepClone,
  recursiveClone,
  addUrlParams,
  buildApiPath,
  getCompConfigData,
  getAllFuncCompName
}
