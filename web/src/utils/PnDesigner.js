// 此工具类用于构建设计器所使用的源数据的对象模板

import PnUtil from './PnUtil'

const buildInitPageMetadata = function () {
  let pageMetadata = {
    layout: {
      layoutItems: [

      ]
    }
  };
  return pageMetadata
};

/**
 * 构建 绝对布局默认配置
 * @returns
 */
const buildAbsoluteLayoutConfigData = function () {
  let layoutConfigData = { // 绝对布局默认配置
    width: 1024,
    widthPixelUnit: 'px',
    height: 768,
    heightPixelUnit: 'px',
    backgroundColor: '#0E2B43',
    showGrid: false,
    canvasGridClass: 'canvas-grid-white-10px10px',
    dragPixel: 1,  // 拖拽单位像素

    imageRelativePath: '',
    imageRepeat: 'no-repeat',
    imageSize: '100% 100%',

    customStyleCode: '{\n}'
  };
  return layoutConfigData
};

/**
 * 构建 绝对布局布局块对象
 * @param payload
 * @returns
 */
const buildAbsoluteLayoutItem = function (name = '', customLayoutItemConfigData = null) {
  let layoutItem = {
    id: PnUtil.uuid(),
    name: name,
    aliasName: '',
    layoutItemConfigData: {
      draggableEnabled: true,
      resizableEnabled: true,

      width: 250,
      height: 250,
      left: 0,
      top: 0,

      borderWidth: 0,
      borderStyle: 'solid',
      borderColor: '#000',

      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,

      zIndex: 2,

      animationVisible: false,
      animationDelay: '',
      inAnimation: '',
      outAnimation: '',

      rotationAngle: 0, // 旋转角度
      flipHorizontal: false, // 水平翻转
      flipVertical: false, // 垂直翻转

      display: 'block'
    },
    component: {
      id: '',
      name: '',
      compConfigData: {

      }
    }
  };

  if (customLayoutItemConfigData) {
    Object.assign(layoutItem.layoutItemConfigData, customLayoutItemConfigData)
  }

  return layoutItem
};

/**
 * 构建 响应式布局默认配置
 * @returns
 */
const buildReactiveLayoutConfigData = function () {
  let layoutConfigData = {
    width: 100,
    widthPixelUnit: '%',
    height: 100,
    heightPixelUnit: '%',
    backgroundColor: '#999999',
    gridItemMargin: 10,
    responsive: false,
    draggable: true,
    resizable: true,

    imageRelativePath: '',
    imageRepeat: 'no-repeat',
    imageSize: '100% 100%',

    customStyleCode: '{\n}'
  };
  return layoutConfigData
};

/**
 * 构建 响应式布局布局块对象
 * @returns
 */
const buildReactiveLayoutItem = function (name = '', customLayoutItemConfigData = null) {
  let layoutItem = {
    id: PnUtil.uuid(),
    name: name,
    aliasName: '',
    layoutItemConfigData: {
      x: 0,
      y: 0,
      w: 6,
      h: 6,

      borderWidth: 0,
      borderStyle: 'solid',
      borderColor: '#000',

      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,

      display: 'block',
    },
    component: {
      id: '',
      name: '',
      compConfigData: {

      }
    }
  };

  if (customLayoutItemConfigData) {
    Object.assign(layoutItem.layoutItemConfigData, customLayoutItemConfigData)
  }

  return layoutItem
};

/**
 * 构建 Echart组件默认配置
 * @returns {{useUrlParam: boolean, apiPath: string}}
 */
const buildEchartConfigDataTemp = function () {
  let configDataTemp = {
    useUrlParam: false,           // 使用URL参数
    apiPath: '',                  // 接口地址
    useApiPath: false,            // 是否调用接口获取数据
    customJsCode: '',             // 自定义JS代码
    onlyUseCustomJsCode: false,   // 只使用自定义JS代码逻辑
  };
  return configDataTemp
};

export default {
  buildInitPageMetadata,
  buildAbsoluteLayoutConfigData,
  buildAbsoluteLayoutItem,
  buildReactiveLayoutConfigData,
  buildReactiveLayoutItem,
  buildEchartConfigDataTemp
}
