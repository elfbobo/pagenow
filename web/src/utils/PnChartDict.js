
const textAlign = [
  {
    label: '自动',
    value: 'auto'
  },
  {
    label: '左对齐',
    value: 'left'
  },
  {
    label: '右对齐',
    value: 'right'
  },
  {
    label: '居中',
    value: 'center'
  },
];

export default {
  textAlign
}
